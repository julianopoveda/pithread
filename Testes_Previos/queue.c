#include <stdlib.h>
#include "queue.h"

//Por no .h
m_queue *initQueues()
{
	m_queue *queue = (m_queue*) malloc(sizeof(m_queue));
	queue->first = NULL;
	queue->last = NULL;
	return queue;
}

//Testar(ok)
//Segue a politica FIFO para desempate de credReal
int InsertQueue(TCB_t *elemento, m_queue *queue)
{
	puts("alocando na fila...");
	//Verifica primeiramente se a fila está vazia
	if(queue->first == NULL)
	{
		elemento->prev = NULL;
		elemento->next = NULL;
		//Inicializa os ponteiros de inicio e fim da lista que será o mesmo neste caso
		queue->first = elemento;
		queue->last = queue->first;
		
		puts("primeiro elemento inserido na fila...");
		return true;
	}
	else
	{
		TCB_t* aux= queue->first;
		//Verificar se o elemento inserido possui prioridade menor ou igual ao primeiro. Se for ajusta o ponteiro de inicio e retorna
		if(elemento->credReal <= aux->credReal)
		{
			queue->first = elemento;
			aux->prev = elemento;
			elemento->next = aux;
			elemento->prev = NULL;
			
			puts("substituicao do elemento inserido no inicio da fila...");
			return true;
		}
		
		//Avança na lista até encontrar a posição que deve ser lançado o valor
		for(aux = queue->first; aux != NULL && elemento->credReal > aux->credReal; aux = aux->next);
			
		//Se saiu do laço significa que todas as prioridades são menores que a do elemento
		//Verifica se já passou pelo último, se sim, atribui o ponteiro do último elemento da fila ao aux
		if(aux == NULL){
			aux = queue->last;//atribui ao aux o ponteiro do último registro
			aux->next = elemento;
			elemento->prev = aux;
			elemento->next = NULL;
			queue->last = elemento;
			puts("substituicao do elemento inserido no fim da fila...");
		}
		else
		{
			TCB_t* prev_element = aux->prev;
			aux->prev = elemento;
			elemento->next = aux;
			elemento->prev = prev_element;
			prev_element->next = elemento;
			puts("elemento inserido no meio da fila...");
		}
		return true;
	}
}

//Implementar. Ver se sera necessario implementar a parte da busca em outra função
//Por padrao vou usar sempre na file de ativos e na de bloqueados se houver
//Testar(ok)
int RemoveQueue(int identificador, m_queue* queue)
{
	TCB_t *aux;
	
	for(aux = queue->first; aux != NULL; aux = aux->next){
		if(aux->id == identificador)
		{
			if(aux != queue->first)
				aux->prev->next = aux->next;
			else
			{
				queue->first = aux->next;
				aux->next->prev = NULL;//se for o primeiro o anterior deve ser o novo primeiro
			}
			
			if(aux != queue->last)
				aux->next->prev = aux->prev;
			else
			{
				queue->last = aux->prev;
				aux->prev->next = NULL;//se for o ultimo o anterior deve ser o novo ultimo
			}
			
			return true;
		}
	}
	
	return false;
}

//Testar(ok)
int InvertQueue()
{
	if(queueInativos->first == NULL && queueAtivos->first == NULL)
		return QueuesEmpty; //Ambas as filas vazias, significa que pode acabar o programa
	
	m_queue *queueAtivosTemp = queueAtivos;
	
	queueAtivos = queueInativos;
	
	//Garantir que não será perdida a lista de inativos
	queueInativos = queueAtivosTemp;
	//Aqui eu zero a fila de Inativos, uma vez que todos os ativos foram execuados
	queueInativos->first = NULL;
	queueInativos->last = NULL;
	
	
	if(queueAtivos->first == NULL)
		return false;//Se ainda estiver vazia, deu erro, pois já verifiquei se as filas estavam vazias
	
	return true;
}

//Testar(ok)
void PrintQueue(m_queue* queue)
{
	TCB_t* aux;
	if(queue==NULL)
		puts("eh nulo");
	/*else
		puts("nao nulo");*/
	
	for(aux = queue->first; aux != NULL; aux = aux->next)
		printf("id: %d, prioridade: %d\n", aux->id, aux->credReal);
	
}