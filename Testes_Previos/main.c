#include <stdio.h>
#include "random.c"
#include "queue.c"

void TestaGenerateIdentifier()
{
	printf("O identificador gerado eh: %d\n", GenerateIdentifier());
}

void TestaGeneratePriority()
{
	printf("A prioridade gerada e a semente eh: %d, %d\n", GeneratePriority(), idCount);
}

void CreateThread(m_queue *queue)
{
	TCB_t* thread = (TCB_t*)malloc(sizeof(TCB_t));
	thread->prev = NULL;
	thread->next = NULL;
	thread->id = GenerateIdentifier();
	thread->credReal = GeneratePriority();
	
	if(thread != NULL)
		InsertQueue(thread, queue);
}

int main()
{
	queueAtivos = initQueues();
	queueInativos = initQueues();
	int i=0;
	for(i= 0; i<10; i++){
		CreateThread(queueAtivos);
		CreateThread(queueInativos);
		//TestaGenerateIdentifier();
		//TestaGeneratePriority();
	}
	
	printf("Imprimindo Fila Ativos...\n");
	PrintQueue(queueAtivos);
	printf("Imprimindo Fila Inativos...\n");
	PrintQueue(queueInativos);
	
	printf("Invertendo Filas...\n");
	InvertQueue();
	printf("Imprimindo Fila Ativos...\n");
	PrintQueue(queueAtivos);
	printf("Imprimindo Fila Inativos...\n");
	PrintQueue(queueInativos);
	
	printf("Removendo Multiplos de 3 da Fila de Ativos...\n");
	TCB_t *aux;
	for (aux = queueAtivos->first; aux !=NULL; aux=aux->next)
		if(aux->id % 3 == 0)
			if(RemoveQueue(aux->id, queueAtivos))
				printf("Thread de id %d removido com sucesso\n", aux->id);
	
	printf("Reimprimindo Fila Ativos...\n");
	PrintQueue(queueAtivos);
	
	exit(0);
}
