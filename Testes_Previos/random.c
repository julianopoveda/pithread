#include <stdlib.h>
#include <time.h>
#include "random.h"

int GenerateIdentifier()
{
//	Não vale a pena usar randomico neste caso. Vou usar contador
//	srand((unsigned)randomSeed);
//	int id = rand();//Inicialmente só retornar o randomico gerado
	int id = idCount++;
	return id;
}

int GeneratePriority()
{
	srand((unsigned)time(NULL)+idCount);
	int priority = (rand()%10)*10;//Inicialmente só retornar o randomico gerado

	//Deve ser garantido que a prioridade não seja zero
	if(priority < 10)
		priority = 10;

//	randomSeed = time(NULL) + priority/10;
	return priority;
}
