#define QueuesEmpty 2

//Criar a  struct aqui para deixar mais simples
typedef struct TCB
{
	int id;
	int credReal;//vai de 0 a 100 em intervalos de 10
	struct TCB *next;
	struct TCB *prev;
} TCB_t;


typedef enum {
	true  = 1, 
	false = 0
} bool;

typedef struct{
	TCB_t *first;
	TCB_t *last;
} m_queue;


m_queue *queueAtivos;
m_queue *queueInativos;
//criar fila bloqueados

int InsertQueue(TCB_t *elemento, m_queue *queue);
int RemoveQueue(int identificador, m_queue *queue);
int InvertQueue();//Inverte os ponteiros quando a fila de ativos esta vazia
void PrintQueue(m_queue* queue);//Imprime a fila solicitada
