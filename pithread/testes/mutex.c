/*
 * mutex.c: realiza operações de soma/subtração na variável global z 
 *           e multiplicação/divisão na variável global t
 *			Exemplo retirado da prova de gerÊncia de processadores 2015/2
 *
 * Compile com: gcc -o mutex mutex.c -lpithread -lm -Wall
 *
 * 
 */
 
#include "../include/pithread.h"
#include <stdio.h>
#include <stdlib.h>

pimutex_t *mutex;

int x, y, z, t;
void* thread1(void *arg) {
    while(x < 4) {
        x = x+1;
    	if (x%3==0) 
    	{
    		printf("thread1 tentando entrar em seção critica\n");
    		if(pilock(mutex) == 0)
    		{
    			printf("thread1 em sessão critica\n");
	    		z = z+1;
	    		t = 2*t;
	    		printf("thread1 saindo da seção critica\n");
	    		piunlock(mutex);
    		}
    	}
	}
 }

void* thread2(void *arg) 
{
	while(y <= 7) 
	{
		y = y+1;
		if (y%7==0) 
		{
			printf("thread2 tentando entrar em seção critica\n");
    		if(pilock(mutex) == 0)
    		{
    			printf("thread2 em sessão critica\n");
				t = t/2;
				z = z-1;
				printf("thread2 saindo da seção critica\n");
	    		piunlock(mutex);
    		}
		}
	}
}

int main()
{
    x = 0;
    y = 0;
    t = 5;
    z = 13;
    
    printf("Inicializando as threads\n");
    int id0 = picreate(50, thread1, (void *)(5));
    int id1 = picreate(30, thread2, (void *)(5));
    
    printf("Inicializando o mutex\n");
    
    if(pimutex_init(mutex) == -1)
    {
    	printf("Erro ao inicializar o mutex\n");
    	exit(-1);
    }
    
    printf("Encerrando a execução e exibindo x = %d\t y = %d\t z = %d\t e t = %d\n",x, y, z, t);
    printf("threads usadas: %d\t%d\n", id0, id1);
    return 0;
}