/*
 * table.c - Implementação do TAD
 */

/*
 * UFRGS : INF01142 : Prof. Sérgio Cechin
 * Biblioteca de threads em nível de usuário
 * Autores: Juliano Nakamura e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/table.h"
#include "../include/pidata.h"

pi_table *thread_table;

/*
 * Aloca uma tabela de threads com 'size' linhas.
 */
pi_table *TableAlloc(int size)
{
	int i;
	
	if(size <= 0) 
	{
		fprintf(stderr, "Erro fatal: tamanho inválido ao alocar tabela de threads.\n");
		exit(-1);
	}
	
	pi_table *table = (pi_table *) malloc(sizeof(pi_table));
	table->row = (pi_table_row *) malloc(size*sizeof(pi_table_row));
	table->size = size;
	
	for(i = 0; i < size; i++) 
		table->row[i].isValid = false;
	
	return table;
}

/*
 * Retorna a thread (ponteiro TCB_t) indexada pela tid.
 * O algoritmo de busca é linear, a tabela é percorrida linha por linha.
 * Caso tid não seja um identificador de thread válido, retorna NULL.
 */
TCB_t *TableGet(int tid)
{
	int i;
	pi_table_row linha;
	
	for(i = 0; i < thread_table->size; i++) 
	{
		linha = thread_table->row[i];
		if(linha.tid == tid && linha.isValid)
			return linha.thread;
	}
	
	return NULL;
}

/*
 * Insere uma thread na tabela.
 * Não verifica se a thread já existe.
 * Se não há mais espaço na tabela é realocada uma com o dobro
 * do tamanho.
 */
bool TableAdd(int tid, TCB_t *thread)
{
	int i;
	pi_table_row *linha;
	
	// Procurando a primeira linha vazia
	for(i = 0; i < thread_table->size; i++) 
	{
		linha = &thread_table->row[i];
		if(!linha->isValid) 
		{
			linha->tid = tid;
			linha->thread = thread;
			linha->isValid = true;
			return true;
		}
	}
	
	// precisa realocar
	// aloca o dobro do tamanho original
	int sizeOld = thread_table->size;
	thread_table->row = (pi_table_row *) realloc(thread_table->row, 2*sizeOld*sizeof(pi_table_row));
	thread_table->size = 2*sizeOld;
	
	// inicializando novo espaço
	for(i = sizeOld; i < thread_table->size; i++) 
		thread_table->row[i].isValid = false;
	
	// e agora a inserção
	thread_table->row[sizeOld].tid = tid;
	thread_table->row[sizeOld].thread = thread;
	thread_table->row[sizeOld].isValid = true;
	
	return true;
}

TCB_t *TableRemove(TCB_t *thread)
{
	int i;
	pi_table_row *linha;
	
	for(i = 0; i < thread_table->size; i++) 
	{
		linha = &thread_table->row[i];
		if(linha->thread == thread && linha->isValid) 
		{
			linha->isValid = false;
			return linha->thread;
		}
	}
	
	return NULL;
}