/*
 * pimutexqueue.c - Implementação do TAD
 */

/*
 * UFRGS : INF01142 : Prof. Sergio Cechin
 * Biblioteca de threads em nível de usuário
 * Autores: Juliano Nakamura e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/pidata.h"
#include "../include/pimutexqueue.h"

/*
 * Adiciona um elemento (thread) na fila.
 * Para adicionar, NÃO é feita uma cópia da thread.
 * Simplismente é adicionado o ponteiro.
 * O campo 'next' da thread é alterado.
 * Por tratar-se de uma FIFO, adiciona-se a thread sempre no último elemento da fila
 */
bool MutexQueueAppend(pimutex_t *mutex, TCB_t *thread)
{
	//Verifica primeiramente se a fila está vazia
	if(MutexQueueIsEmpty(mutex))
	{
		mutex->first = thread;
		mutex->last = thread;
		thread->prev = NULL;
		thread->next = NULL;
	}
	else
	{
		//Como é FIFO, posso realizar sempre a inserção pelo último. Torna a implementação mais eficiente
		thread->next = NULL;//garante que o ponteiro do próximo do último será sempre nulo
		mutex->last->next = thread;
		thread->prev = mutex->last; //aponta o anterior da nova thread para o atual último
		mutex->last = thread;
	}
	return true;//Se correu tudo bem retorna true
}

/*
 * Remove e retorna o primeiro elemento (thread) que esta aguardando no mutex.
 * Se a fila estiver vazia retorna NULL.
 */
TCB_t *MutexQueueRemove(pimutex_t *mutex)
{
	TCB_t *thread;
	
	if(MutexQueueIsEmpty(mutex))
		return NULL;
	
	//Se o primeiro e o último são iguais, significa que só tem um elemento na lista
	if(mutex->first == mutex->last)
	{
		//Extrai da lista
		thread = mutex->first;
		//Resetar os ponteiros da lista de threads na espera
		mutex->last = NULL;
		mutex->first = NULL;
	}
	else
	{
		thread = mutex->first;
		//Atualiza o ponteiro do first. Se ele for o último já estará atualizado
		mutex->first = thread->next;
		//Atualiza o ponteiro de anterior para remover a referência da thread que esta sendo removida
		mutex->first->prev = NULL;
	}
	
	//Resetar ponteiros next e prev
	thread->prev = NULL;
	thread->next = NULL;
		
	return thread;
}

/*
 * Testa se a fila está vazia.
 * Por convenção testamos as duas pontas, mas se first for NULL
 * a fila está deve estar vazia também.
 */
bool MutexQueueIsEmpty(pimutex_t *mutex)
{
	if(mutex->first == NULL && mutex->last !=NULL)
	{
		fprintf(stderr, "Erro fatal no Mutex: fila de thread inconsistente\n");
		exit(-1);
	}
	
	if(mutex->first == NULL && mutex->last == NULL)
		return true;
	else
		return false;
}