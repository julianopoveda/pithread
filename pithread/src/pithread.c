/*
 * pithread.c
 * Arquivo principal da biblioteca. As funções da API são implementadas aqui
 */

/*
 * UFRGS : INF01142 : Prof. Sérgio Luís Cechin
 * Biblioteca de threads em nível de usuário
 * Autores: Juliano Nakamura e Juliano Poveda
 */
 
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
 
#include "../include/global.h"
#include "../include/pithread.h"
#include "../include/pidata.h"
#include "../include/context.h"
#include "../include/scheduler.h"
#include "../include/queue.h"
#include "../include/table.h"
#include "../include/piwaitqueue.h"
#include "../include/pimutexqueue.h"
 
#define S_STACK_SIZE (10*SIGSTKSZ)
#define BONUS_CRED 20 //creditos de bônus
#define DESCONTO_CRED 10//creditos de desconto
#define SUCCESS 0
#define ERROR -1


static int is_pithread_initialized = false;
static int thread_count;


//--------Região Global--------//
/*
 * Função que inicializa todas as estruturas de dados da biblioteca.
 * Só deve ser chamada pela picreate(), as outras funções da API devem
 * verificar se 'is_pithread_initialized', e caso não, geram um erro.
 */
void InitPiThread()
{
	if(!is_pithread_initialized)
	{
		thread_count = 0;
		ativos_queue = QueueAlloc();
		inativos_queue = QueueAlloc();
		thread_table = TableAlloc(16);
		//Inicializa a fila de wait
		wait_queue = WaitQueueAlloc();
		
		// Criando o contexto para a função de cleanup de threads
		getcontext(&thread_destroy_context);
		thread_destroy_context.uc_link = NULL; // nunca é executado. Escalonador entra antes
		thread_destroy_context.uc_stack.ss_sp   = (char *) malloc(S_STACK_SIZE);
		thread_destroy_context.uc_stack.ss_size = S_STACK_SIZE;
		makecontext(&thread_destroy_context, (void (*)(void))ThreadDestroy, 0);
		
		// Criando representação da thread main() na sthread
		main_thread = ThreadCreate(11);//cria a thread main com prioridade inicial de 11(baixa)
		running_thread = main_thread;
		TableAdd((GetRunningThread())->tid, GetRunningThread());//thread main terá tid == 0
		PutInReadyState(GetRunningThread());
		ContextSave(GetRunningThread()); // Salva "aqui" no contexto da thread main()
		
		if(!is_pithread_initialized) 
		{
			is_pithread_initialized = true;
			DispatchThread(Scheduler()); // Escalona... e executa a main :)
		}
	}
}

/*
 * Caso precisemos utilizar um esquema diferente
 * de geração de tid's.
 */
int GenerateIdentifier()
{
    return thread_count++;
}

/*
 * Cria a thread propriamente dita.
 * A maneira como threads são referênciadas pela biblioteca
 * é por meio de ponteiros. Após criada, a thread é inserida
 * em uma tabela que implementada um mapeamente de tids (que são int's)
 * para os ponteiros TCB_t.
 */
TCB_t *ThreadCreate(int creditos)
{
	TCB_t *thread;
	
	thread = (TCB_t *) malloc(sizeof(TCB_t));
	thread->tid = GenerateIdentifier();
	thread->state = STATE_CRIACAO;
	thread->credCreate = creditos < 1? 1: creditos;
	thread->credReal = thread->credCreate;
	
	return thread;
}

/*
 * Desaloca o contexto da thread e libera seus recursos.
 * Dentre eles, libera a thread que está esperando (que fez piwait).
 * Na inicialização da biblioteca é criado um contexto para essa função.
 * Quando criando o contexto para qualquer thread, esse contexto (desta função aqui)
 * é colocado no uc_link. Assim, quando a thread terminar, essa função será executada.
 * OBS.: essa função não é executada para a "thread" main.
 */
void ThreadDestroy()
{
	TCB_t *thisThread, *threadWaiting;
	
	thisThread = GetRunningThread();
	threadWaiting = WaitQueueRemove(wait_queue, thisThread);

	if(threadWaiting != NULL) 
	{
		//Adiciona o boônus por sair do estado de bloqueado
		if(threadWaiting->credReal > 10) //senão devolve para o estado de aptos, pois os créditos ainda não expiraram.
			PutInReadyState(threadWaiting);
		else
		{
			if(!QueueAppend(inativos_queue, thisThread))
			{
				fprintf(stderr, "Erro fatal em ThreadDestroy: thread que deveria ir para fila de expirados não conseguiu\n");
				exit(ERROR);
			}
		}
	}
	
	// Removendo do mapeamento...
	TableRemove(thisThread);
	
	if(thisThread->state == STATE_APTO) 
	{
		fprintf(stderr, "Erro fatal em ThreadDestroy: thread que está terminando não poderia estar em STATE_APTO\n");
		exit(ERROR);
	}
	
	// Segue o baile
	DispatchThread(Scheduler());
}

//manipular os créditos da thread
void ManageCreditos(TCB_t *thread, pi_creditos_operation operacao)
{
	int creditosAtuais;
	
	switch(operacao)
	{
		case OPERATION_RESET://Reinicia os crétidos originais
			thread->credReal = thread->credCreate;
			break;
		case OPERATION_REMOVE:
			thread->credReal -= DESCONTO_CRED;
			break;
		case OPERATION_BONUS:
			creditosAtuais = thread->credReal;//facilitar a leitura e testes
			//Se a soma passar de 100 tenho que adicionar somente o quanto falta para chegar a 100
			if(creditosAtuais + BONUS_CRED > 100)
				thread->credReal += 100 - creditosAtuais;
			else
				thread->credReal += BONUS_CRED;
			break;
	}
}
//--------Fim Região Global--------//

//--------Região Pithread--------//
int picreate (int credCreate, void* (*start)(void*), void *arg)
{
    TCB_t *thread;
	
	InitPiThread();
	
	// Fila de aptos ativos correspondente não existe
	if(ativos_queue == NULL) 
		return ERROR;
	
	thread = ThreadCreate(credCreate);
	
	// Quando a thread sendo criada terminar (a função terminar de executar)
	// o próximo contexto a executar será o da função responsável por fazer
	// o cleanup da thread (vide função ThreadDestroy()).
	getcontext(&thread->context);
	thread->context.uc_link = &thread_destroy_context;
	thread->context.uc_stack.ss_sp   = (char *) malloc(S_STACK_SIZE);
	thread->context.uc_stack.ss_size = S_STACK_SIZE;
	makecontext(&thread->context, (void (*)(void)) start, 1, arg);
	
	TableAdd(thread->tid, thread);
	
	PutInReadyState(thread);
	
	// A decisão de projeto foi não chamar o escalonador
	// ao criar uma nova thread. Então esta thread continuará
	// sua execução normal (picreate não "cutuca" o escalonador).
	
	return thread->tid;
}

int piyield()
{
    bool hasYielded = false;
	TCB_t *thisThread;
	
	if(!is_pithread_initialized) 
		return ERROR;
	
	// Quando esta thread voltar a executar, hasYielded vai ser true
	thisThread = GetRunningThread();
	
	ContextSave(thisThread);
	
	//verifica se a quantidade de créditos expirou. Se sim adiciona na fila de expirados
	if(!hasYielded) 
	{
		hasYielded = true; // É alterado mesmo com o contexto salvo.
		/*Verifica se o valor é maior que 10. As subtrações devem ser realizadas somente no Scheduler()
		* Aqui irá só definir para qual fila a thread deve ser adicionada.
		*/
		
		DispatchThread(Scheduler()); // Escalona e roda outra thread
		return ERROR; // Nunca deve ser executado
	} 
	else 
	{
		thisThread = GetRunningThread();
		return SUCCESS;
	}
}

int piwait(int tid)
{
    TCB_t *thread, *thisThread;
	bool waitDone = false;
	
	if(!is_pithread_initialized)
		return ERROR;
	
	thread = TableGet(tid);

	if(thread == NULL)
		return ERROR;

	thisThread = GetRunningThread();

	if(thread == thisThread || thread == main_thread)
		return ERROR;
	
	//Se passar daqui significa que consegui adicionar a thread, não preciso repetir o processo
	if(!WaitQueueAppend(wait_queue, thread, thisThread))
		return ERROR;

	thisThread->state = STATE_BLOQUEADO;
	ContextSave(thisThread);

	if(!waitDone) 
	{
		waitDone = true;
		DispatchThread(Scheduler());
	}
	
	return SUCCESS;
}

//Implementado
int pimutex_init(pimutex_t *mtx)
{
	mtx = (pimutex_t *) malloc(sizeof(pimutex_t));
    mtx->flag = MUTEX_LIVRE;
    mtx->first = NULL;
    mtx->last = NULL;
 
    if(mtx == NULL)
    	return ERROR;
    else
    	return SUCCESS;
}

int pilock (pimutex_t *mtx)
{
	bool insert;
	TCB_t *thisThread = GetRunningThread();

	if(!is_pithread_initialized)
		return ERROR;
	
	// Mutex não inicializado (alocado)
	if(mtx == NULL) 
		return ERROR;
	
	// Teste Flag em estado incosistente
	if(mtx->flag != MUTEX_LIVRE && mtx->flag != MUTEX_OCUPADO)
		return ERROR;
	
	ContextSave(thisThread);
	
	// Quando essa thread sair da fila de espera do lock, for
	// desbloqueda e entrar numa das filas de STATE_APTO, a
	// sua execução vai continuar a partir daqui. Ela vai ficar
	// num loop até conseguir entrar.
	
	//Tenta usar o mutex. Se já estiver ocupado adiciona na lista respectiva
	if(mtx->flag == MUTEX_LIVRE)
		mtx->flag = MUTEX_OCUPADO; // Agora tem alguém no mutex!
	else 
	{
		// Já tem alguém no mutex. Bloqueia essa thread
		// e a coloca na fila de espera desse mutex.
		thisThread->state = STATE_BLOQUEADO;
		insert = MutexQueueAppend(mtx, thisThread);
		if(!insert) 
			exit(ERROR);
		
		// Segue o baile com outra thread
		DispatchThread(Scheduler());
	}
	
	return SUCCESS;
}

int piunlock (pimutex_t *mtx)
{
	TCB_t *unlocked;

	if(!is_pithread_initialized) 
		return ERROR;
	
	unlocked = MutexQueueRemove(mtx);//Desbloqueia a primeira thread que esta esperando
	
	// Se tem alguém bloqueado nesse lock desbloqueia
	if(unlocked != NULL) 
	{
		//Verifica se vai para aptos ativos ou aptos expirados. Somente depois adiciona o bônus
		if(unlocked->credReal > DESCONTO_CRED)
			PutInReadyState(unlocked);
		else
		{
			if(!QueueAppend(inativos_queue, unlocked))
			{
				fprintf(stderr, "Erro fatal em piunlock: thread que deveria ir para fila de expirados não conseguiu\n");
				exit(ERROR);
			}
		}
		//Adiciona o bônus efetivamente
		ManageCreditos(unlocked, OPERATION_BONUS);
	}
	
	//Libera a flag do mutex
	mtx->flag = MUTEX_LIVRE;
	
	return SUCCESS; 
}
//--------Fim Região Pithread--------//