/*
 * queue.c - Implementação do TAD
 */

/*
 * UFRGS : INF01142 : Prof. Sergio Cechin
 * Biblioteca de threads em nível de usuário
 * Autores: Juliano Nakamura e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/pidata.h"
#include "../include/queue.h"

/*
 * Aloca uma fila e inicializa os ponteiros com NULL's (fila vazia).
 */
pi_queue *QueueAlloc()
{
	pi_queue *queue = (pi_queue*) malloc(sizeof(pi_queue));
	queue->first = NULL;
	queue->last  = NULL;
	return queue;
}

/*
 * Adiciona um elemento (thread) na fila.
 * Para adicionar, NÃO é feita uma cópia da thread.
 * Simplismente é adicionado o ponteiro.
 * O campo 'next' da thread é alterado.
 */
bool QueueAppend(pi_queue *queue, TCB_t *thread)
{
	//Verifica primeiramente se a fila está vazia
	if(QueueIsEmpty(queue))
	{
		thread->prev = NULL;
		thread->next = NULL;
		
		//Inicializa os ponteiros de inicio e fim da lista que será o mesmo neste caso
		queue->first = thread;
		queue->last = thread;
		
		return true;
	}
	else
	{
		TCB_t* aux= queue->first;
		/*Verificar se o elemento inserido possui prioridade menor ou igual ao primeiro. 
		* Se for ajusta o ponteiro de inicio e retorna
		*/
		if(thread->credReal <= aux->credReal)
		{
			queue->first = thread;
			aux->prev = thread;
			thread->next = aux;
			thread->prev = NULL;
			
			return true;
		}
		
		//Avança na lista até encontrar a posição que deve ser lançado o valor
		for(aux = queue->first; aux != NULL && thread->credReal > aux->credReal; aux = aux->next);
			
		//Se saiu do laço significa que todas as prioridades são menores que a do thread
		//Verifica se já passou pelo último, se sim, atribui o ponteiro do último thread da fila ao aux
		if(aux == NULL){
			aux = queue->last;//atribui ao aux o ponteiro do último registro
			aux->next = thread;
			thread->prev = aux;
			queue->last = thread;
			thread->next = NULL;
		}
		else
		{
			TCB_t* prevThread = aux->prev;
			aux->prev = thread;
			thread->next = aux;
			thread->prev = prevThread;
			prevThread->next = thread;
		}
	}
	return true;//Se correu tudo bem retorna true
}

/*
 * Remove e retorna um elemento (thread) de determinada fila.
 * Se a fila estiver vazia retorna NULL.
 */
TCB_t *QueueRemove(pi_queue *queue)
{
	TCB_t *thread;
	if(QueueIsEmpty(queue))
		return NULL;
	
	//Como o anterior retorna se a fila é vazia, posso economizar uma atribuição para o próximo if
	thread = queue->last;

	// Última thread da fila
	if(queue->first == queue->last) { 
		queue->first = NULL;
		queue->last  = NULL;
	} 
	else
	{
		TCB_t* prevThread = thread->prev;//Recupera a penúltima thread
		prevThread->next = NULL;
		queue->last = prevThread; // a ponta da fila é a próxima thread
	}
	
	thread->prev = NULL;
	
	return thread;
}

/*
 * Remove thread de fila. Não remove a thread da ponta (como em uma fila),
 * mas sim a thread igual à trd. Retorna true caso trd exista na fila.
 * foi alterado o parametro de TCB_t trd para int tid. Se der erro aqui nesta função pode ser isso
 */
bool QueueRemoveAsList(pi_queue *queue, TCB_t *trd)
{
	TCB_t *thread;//, *aux
	bool threadMatch = false;
	
	for(thread = queue->first; thread != NULL && threadMatch == false ; thread = thread->next)
		if(thread == trd)
			threadMatch = true;
	
	if(threadMatch) {
		if(thread == queue->last)
			QueueRemove(queue);
		else if(thread == queue->first) 
		{
			TCB_t *nextThread = thread->next; //Facilitar a leitura
			queue->first = nextThread;
			nextThread->prev = NULL;
		}
		else 
		{
			TCB_t *auxThread = thread->prev;
			auxThread->next = thread->next;
			auxThread = thread->next;
			auxThread->prev = thread->prev;
		}
		
		thread->prev = NULL;
		thread->next = NULL;
		
		return true;
	} 
	else 
		return false;
}

/*
 * Testa se a fila está vazia.
 * Por convenção testamos as duas pontas, mas se first for NULL
 * a fila está deve estar vazia também.
 */
bool QueueIsEmpty(pi_queue *queue)
{
	if(queue == NULL)
	{
		fprintf(stderr, "Erro fatal: fila não alocada em queueIsEmpty.\n");
		exit(-1);
	}
	
	if(queue->first == NULL && queue->last == NULL)
		return true;
	else
		return false;
}

/*
 *Inverte os ponteiros das filas passadas. 
 *Será utilizada quando a fila de aptos ativos estiver vazia
*/
bool QueueInvert(pi_queue *queueAtivo, pi_queue *queueExpirado)
{
	//Apenas troca os ponteiros do primeiro e do último.
	queueAtivo->first = queueExpirado->first;
	queueAtivo->last =queueExpirado->last;
	
	//Resetar a fila de Inativos, uma vez que todos os ativos foram execuados
	queueExpirado->first = NULL;
	queueExpirado->last = NULL;
	
	
	if(queueAtivo->first == NULL)
	{
		fprintf(stderr, "Erro Fatal: fila esta vazia quando nao deveria estar");
		exit(-1);//Se ainda estiver vazia, deu erro, pois já verifiquei se as filas estavam vazias
	}

	return true;
}

/*
 * Função para debug.
 * Imprime as TID e os Créditos dos elementos de uma fila.
 */
void QueuePrint(pi_queue *queue, const char *nomeFila)
{
	TCB_t *aux;
	
	if(QueueIsEmpty(queue)) {
		printf("%s: vazio\n", nomeFila);
		return;
	}
	
	printf("Printing Queue: %s\n", nomeFila);
	printf("[first]-> ");
		
	for(aux = queue->first; aux != NULL; aux = aux->next)
		printf("tid: %d, priority: %d\n", aux->tid, aux->credReal);
	
	printf(" <-[last]\n");
}

/*
 * Função para debug.
 * Imprime as TID e os Créditos dos elementos de uma fila pelo fim.
 */
void QueuePrintReverse(pi_queue *queue, const char *nomeFila)
{
	TCB_t *aux;
	
	if(QueueIsEmpty(queue)) {
		printf("%s: vazio\n", nomeFila);
		return;
	}
	
	printf("Printing Queue: %s\n", nomeFila);
	printf("[last]-> ");
	
	for(aux = queue->last; aux != NULL; aux = aux->prev)
		printf("tid: %d, priority: %d\n", aux->tid, aux->credReal);
	
	printf(" <-[first]\n");
}