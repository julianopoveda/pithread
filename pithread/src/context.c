/*
 * context.c - Funções diversas para lidar com contextos de execução
 */

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

#include "../include/context.h"
#include "../include/scheduler.h"
#include "../include/queue.h"
#include "../include/global.h"
#include "../include/pidata.h"

ucontext_t runningContext;

/*
 * Contexto que será setada para a função ThreadDestroy().
 * Essa função limpa os recursos da thread que terminou
 * e chama o Scheduler() pra rodar a próxima thread.
 */
ucontext_t thread_destroy_context;

// Armazena uma referência thread "main" (feito na inicialização)
TCB_t *main_thread;


/*
 * Retorna a thread que está rodando correntemente.
 * Logo que possível têm que ser criado o contexto da main().
 */
TCB_t *GetRunningThread()
{
	return running_thread;
}

/*
 * Coloca a thread para executar.
 * É assumido que a thread já foi retirada das filas
 * de STATE_APTO, ou seja, que a thread tenha vindo
 * do Scheduler().
 */
void DispatchThread(TCB_t *thread)
{
	running_thread = thread; // Atualiza variável global
	thread->state = STATE_EXECUCAO;
	setcontext(&thread->context);
}

/*
 * Salva o contexto atual na thread.
 */
void ContextSave(TCB_t *thread)
{
	getcontext(&thread->context);
}

/*
 * Coloca a thread numa das filas de Aptos.
 * Caso a prioridade da thread seja inválida, o programa
 * é abortado.
 */
void PutInReadyState(TCB_t *thread)
{
	if(ativos_queue == NULL) {
		fprintf(stderr, "Erro fatal em PutInReadyState: prioridade inválida\n");
		exit(-1);
	}
	thread->state = STATE_APTO;
	QueueAppend(ativos_queue, thread);
}