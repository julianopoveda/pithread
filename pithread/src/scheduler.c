/*
 * scheduler.c
 */

/*
 * UFRGS : INF01142 : Prof. Sérgio Cechin
 * Biblioteca de threads em nível de usuário
 * Autores: Juliano Nakamura e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

#include "../include/pidata.h"
#include "../include/global.h"
#include "../include/queue.h"
#include "../include/context.h"
#include "../include/scheduler.h"
#include "../include/table.h"

#define ERROR -1
// O escalonador usa duas filas de prioridades
pi_queue *ativos_queue;
pi_queue *inativos_queue;

//Fila de espera. Contém todas as thread que estão sendo esperadas por alguém
piwait_queue *wait_queue;

TCB_t *running_thread;

/* 
 * Decisão de projeto: não vai ter fila de bloqueados.
 * As únicas formas da thread ir para STATE_BLOQUEADO é por
 * meio das chamadas piwait() e pilock(). Toda vez que uma thread
 * terminar (via código de cleanup) ou for desbloqueada de um lock
 * a thread correspondente é recuperada pelas proprias funções.
 */

/*
 * Escolhe uma thread para executar.
 * Sempre vai haver pelo menos uma thread (pelo menos a main()).
 * A thread é removida da correspondente fila de Aptos Ativos.
 */
TCB_t *Scheduler()
{
	TCB_t *thread;
	TCB_t *runningThread = GetRunningThread();//Recupero, pois só posso por essa thread pra executar se a fila estiver vazia
	//Se a thread ainda existir na tabela de thread, realiza a operação de manipulação de créditos, caso contrário nada faz, pois a thread não existe mais.
	if(TableGet(runningThread->tid)!=NULL){
		//Remove 10
		ManageCreditos(runningThread, OPERATION_REMOVE);
		//Reliza a operação aqui.
		if(runningThread->credReal <= 0)
		{
			ManageCreditos(runningThread, OPERATION_RESET);
			if(!QueueAppend(inativos_queue, runningThread))
				{
					fprintf(stderr, "Erro fatal em Scheduler: thread que deveria ir para fila de expirados não conseguiu\n");
					exit(ERROR);
				}
		}
		else
			PutInReadyState(runningThread);
			
	}
	//Verifica se a fila de aptos ativos não esta vazia. Se tiver realiza a segunda verificação
	if(!QueueIsEmpty(ativos_queue))
		thread = QueueRemove(ativos_queue);
	else {
		if(QueueIsEmpty(inativos_queue)) {
			fprintf(stderr, "Erro fatal em scheduler: não há nenhuma thread READY para escalonar.\n");
			exit(-1);
		}
		//Se não estiver vazia a fila de inativos(ativos expirados) entra no lugar da fila de ativos como nova fila de ativos
		else
			QueueInvert(ativos_queue, inativos_queue);

		thread = QueueRemove(ativos_queue);
	}
	
	thread->state = STATE_EXECUCAO;
	return thread;
}
