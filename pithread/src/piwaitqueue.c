/*
 * piwaitqueue.c - Implementação do TAD
 */

/*
 * UFRGS : INF01142 : Prof. Sergio Cechin
 * Biblioteca de threads em nível de usuário
 * Autores: Juliano Nakamura e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/pidata.h"
#include "../include/scheduler.h"
#include "../include/piwaitqueue.h"

/*
 * Aloca uma fila e inicializa os ponteiros com NULL's (fila vazia).
 */
piwait_queue *WaitQueueAlloc()
{
	piwait_queue *wqueue = (piwait_queue*) malloc(sizeof(piwait_queue));
	wqueue->prev = NULL;
	wqueue->next  = NULL;
	wqueue->waiting = NULL;
	wqueue->waited = NULL;
	return wqueue;
}

/*
 * Adiciona um elemento (thread) na fila.
 * Para adicionar, NÃO é feita uma cópia da thread.
 * Por tratar-se de uma FIFO, o novo elemento é sempre adicionado ao fim da fila
 * Simplismente é adicionado o ponteiro.
 * O campo 'next' da thread é alterado.
 */
bool WaitQueueAppend(piwait_queue *wqueue, TCB_t *waitedThread, TCB_t *waitingThread)
{
	//Primeiro checa se a thread que é para ser esperada já esta sendo esperada por alguém
	if(CheckWaitedThread(waitedThread))
		return false;//Significa que já tem alguém esperando por ela
	else
	{
		//Reseta os ponteiros de ambas as threads
		waitingThread->prev = NULL;
		waitingThread->next = NULL;
		
		//Verifica primeiramente se a fila está vazia
		if(WaitQueueIsEmpty(wqueue))
		{
			wqueue->waited = waitedThread;//adicona a thread em questão
			wqueue->waiting = waitingThread;
		}
		else
		{
			piwait_queue* aux= wqueue;
			piwait_queue* newQueue = WaitQueueAlloc();
			//para saber o anterior
			for(aux = wqueue; aux->next != NULL; aux = aux->next);
			
			newQueue->waited = waitedThread;//adicona a thread em questão
			newQueue->waiting = waitingThread;
			
			newQueue->prev = aux;
			aux->next = newQueue;//Adiciona a nova fila como o next de queue
		}
		return true;//Se correu tudo bem retorna true
	}
}

/*
 * Retorna a thread que estava esperando a thread atual.
 * Remove e retorna o elemento (thread) que estava esperando a waitedThread terminar.
 */
TCB_t *WaitQueueRemove(piwait_queue *wqueue, TCB_t *waitingThread)
{
	TCB_t *waitedThread;//waitingThread
	piwait_queue * auxWaitQueue;
	//Vou checar isso um nível acima por garantia
	if(WaitQueueIsEmpty(wqueue)) 
	{
		fprintf(stderr, "Erro fatal em WaitQueueRemove: A fila não pode estar vazia\n");
		exit(-1);
	} 
	
	for(auxWaitQueue = wqueue; auxWaitQueue != NULL && auxWaitQueue->waiting != waitingThread; auxWaitQueue = auxWaitQueue->next);
	
	//Se a fila for nula significa que esta thread não era esperada por ninguém
	if(auxWaitQueue == NULL)
		return NULL;
	
	//Se a thread que esta executando não foi localizada deve ocorrer erro.
	if(auxWaitQueue->waiting != waitingThread)
	{
		fprintf(stderr, "Erro fatal em WaitQueueRemove: Thread esperada não encontrada. Dado inconsistente\n");
		exit(-1);
	}
	else
	{
		//Recupera a thread que estava esperando
		waitedThread = wqueue->waited;
		
		//Última thread da fila
		if(auxWaitQueue->prev == NULL && auxWaitQueue->next == NULL) {
			wqueue->prev = NULL;
			wqueue->next  = NULL;
			wqueue->waiting = NULL;
			wqueue->waited = NULL;
		} 
		else 
		{
			piwait_queue* prevElement = auxWaitQueue->prev;	
			piwait_queue* nextElement = auxWaitQueue->next;
			
			/*Fazer verificações para ver se esta pegando o primeiro ou o último de uma fila preenchida
			 *Não tem problema de atribuir o outro mesmo sem saber o resultado deste, 
			 *pois se o prev ou o next forem NULL vai ser atribuido NULL*/
			if(prevElement != NULL)
				prevElement->next = nextElement;
			if(nextElement != NULL)
				nextElement->prev = prevElement;
			
			//Reseta os valores dos ponteiros
			waitingThread->next = NULL;
			waitingThread->prev = NULL;
		}
	}
	return waitedThread;
}

/*
 * Testa se a fila está vazia.
 * Por convenção testamos as duas pontas, mas se first for NULL
 * a fila está deve estar vazia também.
 */
bool WaitQueueIsEmpty(piwait_queue *wqueue)
{
	if(wqueue->next == NULL && wqueue->prev !=NULL)
	{
		fprintf(stderr, "Erro fatal na Fila wait_queue: fila inconsistente\n");
		exit(-1);
	}
	
	if(wqueue->next == NULL && wqueue->prev == NULL)
		/*Não vejo a necessidade de fazer este teste acima. 
		 *Se os ponteiros de next e prev forem diferentes de null 
		 *os ponteiros waiting e waited obrigatoriamente estarão preenchidos
		*/
		if(wqueue->waiting == NULL && wqueue->waited == NULL)
			return true;
		else
			return false;
	else
		return false;
}

/*
* Verificar a existencia da Thread na fila.
* Se já estiver na fila, significa que já tem alguém esperando pelo término dessa.
* Nesse caso deve retornar true;
*/
bool CheckWaitedThread(TCB_t *waitedThread)
{
	piwait_queue * auxWaitQueue;
	
	for(auxWaitQueue = wait_queue; auxWaitQueue != NULL; auxWaitQueue = auxWaitQueue->next)
		if(auxWaitQueue->waited == waitedThread)
			return true;//encontrou e retorna, terminando assim o laço
	
	return false;//varreu tudo e não encontrou
}

void WaitQueuePrint(piwait_queue *queue)
{
	piwait_queue *aux;
	
	if(WaitQueueIsEmpty(queue))
		printf("fila vazia\n");
	else
		 for(aux = queue; aux !=NULL; aux = aux->next)
            printf("trhead esperada: %d, thread esperando: %d\n", aux->waited->tid, aux->waiting->tid);
}