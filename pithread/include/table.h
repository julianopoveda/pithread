/*
 * table.h
 * Implementa a table que mapeia tid para ponteiros TCB_t.
 */

#ifndef __PI_TABLE__
#define __PI_TABLE__

#include "../include/pidata.h"

typedef struct {
	int tid;
	TCB_t *thread;
	bool isValid; // se linha contém valor válido
} pi_table_row;

typedef struct {
	pi_table_row *row;
	int size;
} pi_table;

extern pi_table *thread_table;

pi_table *TableAlloc(int size);
TCB_t *TableGet(int tid);
bool TableAdd(int tid, TCB_t *thread);
TCB_t *TableRemove(TCB_t *thread);

#endif //__PI_TABLE__