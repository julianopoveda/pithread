/*
 * pimutexqueue.h
 * Arquivo de cabeçalho para o TAD das filas.
 */

#ifndef __PIMUTEX_QUEUE__
#define __PIMUTEX_QUEUE__

#include "../include/pidata.h"
#include "../include/pithread.h"

TCB_t *MutexQueueRemove(pimutex_t *mutex);
bool MutexQueueIsEmpty(pimutex_t *mutex);
bool MutexQueueAppend(pimutex_t *mutex, TCB_t *thread);

#endif //__PIMUTEX_QUEUE__