/*
 * context.h
 */

#ifndef __PI_CONTEXT_H__
#define __PI_CONTEXT_H__

#include <ucontext.h>

#include "../include/pidata.h"

extern TCB_t *running_thread;
extern TCB_t *main_thread;
extern ucontext_t thread_destroy_context;

TCB_t *GetRunningThread();
void ContextSave(TCB_t *thread);
void DispatchThread(TCB_t *thread);
void PutInReadyState(TCB_t *thread);

#endif //__PI_CONTEXT_H__
