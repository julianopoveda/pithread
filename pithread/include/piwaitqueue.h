/*
 * Arquivo de cabeçalho para o TAD das filas para o wait.
 */

#ifndef __PI_WAIT_QUEUE__
#define __PI_WAIT_QUEUE__

#include "../include/pidata.h"

piwait_queue *WaitQueueAlloc();
TCB_t *WaitQueueRemove(piwait_queue *wqueue, TCB_t *waitedThread);
bool WaitQueueIsEmpty(piwait_queue *wqueue);
bool WaitQueueAppend(piwait_queue *wqueue, TCB_t *waitedThread, TCB_t *waitingThread);
bool CheckWaitedThread(TCB_t *waitedThread);
void WaitQueuePrint(piwait_queue *queue);
#endif //__PI_WAIT_QUEUE__