/*
 * scheduler.h
 */

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include "../include/pidata.h"

void InitPiThread();
int GenerateIdentifier();
TCB_t *ThreadCreate(int creditos);
void ThreadDestroy();
void ManageCreditos(TCB_t *thread, pi_creditos_operation operacao);

#endif //__GLOBAL_H__