/*
 * scheduler.h
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include "../include/pidata.h"

#include "queue.h"

// Estas variáveis globais estão declaradas em scheduler.c

// O escalonador usa duas filas: Ativos e Expirados
extern pi_queue *ativos_queue;
extern pi_queue *inativos_queue;


//Fila de espera. Contém todas as threads que estão sendo esperadas por alguém.
extern piwait_queue *wait_queue;

//Thread que esta executando no momento
extern TCB_t *running_thread;

TCB_t *Scheduler();

#endif //__SCHED_H__