/*
 * queue.h
 * Arquivo de cabeçalho para o TAD das filas.
 * É um arquivo de cabeçalho privado, por isso não
 * está no diretório 'include'.
 */

#ifndef __PI_QUEUE__
#define __PI_QUEUE__

#include "../include/pidata.h"

pi_queue *QueueAlloc();
TCB_t *QueueRemove(pi_queue *queue);//Remove a última thread(mais prioritária) e a retorna. Vai ser utilizado para pegar a próxima thread de execução. função otimizada para o dispatcher
bool QueueRemoveAsList(pi_queue *queue, TCB_t *trd);//Remove Queue//Utilizado a principio só em testes
bool QueueIsEmpty(pi_queue *queue);//Verificador de fila vazia. Torna a verificação centrailzada, ver se realmente será necessária
bool QueueAppend(pi_queue *queue, TCB_t *thread);//InsertQueue
void QueuePrint(pi_queue *queue, const char *nomeFila);//Impressão da fila. Função serve como teste
void QueuePrintReverse(pi_queue *queue, const char *nomeFila);//Semelhante a acima, porém imprime do fim
bool QueueInvert(pi_queue *queueAtivo, pi_queue *queueExpirado); //Inverter as filas de aptos ativos e aptos expirados. Será usado no scheduler

#endif //__PI_QUEUE__