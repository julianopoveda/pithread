/*
 * pidata.h: arquivo de inclusã de uso apenas na geração da libpithread
 *
 * Esse arquivo pode ser modificado. ENTRETANTO, deve ser utilizada a TCB fornecida.
 *
 */
#ifndef __pidata__
#define __pidata__

#include <ucontext.h>

/* NÃO ALTERAR ESSA struct */
typedef struct TCB { 
	int	tid; 			// identificador da thread
	int	state;			// estado em que a thread se encontra
					// 0: Criação; 1: Apto; 2: Execução; 3: Bloqueado e 4: Término
	int	credCreate;		// créditos de criação da thread (atribuído na criaçãp
	int	credReal; 		// créitos atuais da thread (usado para determinar a prioridade atual da thread)
	ucontext_t 	context;	// contexto de execução da thread (SP, PC, GPRs e recursos) 
	struct TCB 	*prev; 		// ponteiro para o TCB anterior da lista
	struct TCB 	*next; 		// ponteiro para o próximo TCB da lista
} TCB_t; 

//Definição de Booleano. Facilitar a programação
typedef enum {
	true  = 1, 
	false = 0
} bool;

/*Enumerador de Estados que irão na variável state da TCB. 
* Melhorar a legibilidade do código
*/
typedef enum {
	STATE_CRIACAO     = 0,
	STATE_APTO   = 1,
	STATE_EXECUCAO = 2,
	STATE_BLOQUEADO = 3,
	STATE_TERMINO   = 4
} pi_state;

//Enumerador de Estados do mutex. Tornar o código mais legível
typedef enum {
	MUTEX_OCUPADO = 0,
	MUTEX_LIVRE   = 1
} pi_mutex_state;

//Enumerador de tipos de operação no gerenciamento de Créditos(ManageCréditos)
typedef enum {
	OPERATION_RESET = 0,
	OPERATION_REMOVE = 1,
	OPERATION_BONUS = 2
} pi_creditos_operation;

/*Conterá os ponteiros para o primeiro e último elemento das listas.
* Visa facilitar a manipulação das filas
* O ponteiro first representa a thread de menor prioridade usando a politica de FIFO em caso de empate
* O ponteiro last representa a thread de maior prioridade usando a politica de FIFO em caso de empate
*/
typedef struct{
	TCB_t *first;
	TCB_t *last;
} pi_queue;

/*
* Estrutura que representa a fila de todas as threads que estão sendo aguardadas
* waited: A thread que esta sendo esperada por outra, ou seja, esta na sessão critica
* waiting: A thread que espera o término da thread waited
* prev e next: ponteiros para os elementos anterior e próximo na fila.
*/
typedef struct WAIT_QUEUE{
  	TCB_t *waited;
	TCB_t *waiting;
	struct WAIT_QUEUE *prev;
	struct WAIT_QUEUE *next;
} piwait_queue;

#endif